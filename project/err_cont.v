/* 
   Fanout CDT Design
   C. Lin, 2018.04.11
*/

module err_cont
#( parameter LENGTH_ERR = 21  // w/o headers
  )
///
(
// input 
   clk               , // system clock
   in_live           ,
   in_err            ,
   is_masking        ,
     
// output
   got_tlk_err_bus   ,
   got_dc_err_bus    ,
   
   out_tlk_err_bus   ,
   out_dc_err_bus    ,        
   
);

input  wire         clk;
input  wire         in_live;
input  wire         in_err;
input  wire         is_masking;

output reg          got_tlk_err_bus;
output reg          got_dc_err_bus;

output reg  [20: 0] out_tlk_err_bus;
output reg  [20: 0] out_dc_err_bus;

reg                 got_signal;
reg         [2  :0] pipeline;

reg                 is_tlk_header;
reg                 is_dc_header;

reg         [4  :0] tlk_cnt;
reg         [4  :0] dc_cnt;

reg                 err;

always @(posedge clk)
begin

   if( in_live == 1'b0 )
      begin   
         got_tlk_err_bus = 1'b0;
         got_dc_err_bus = 1'b0;
         
         out_tlk_err_bus = 0;
         out_dc_err_bus = 0;
        
         tlk_cnt = 0;
         dc_cnt = 0;
         
         pipeline = 0;
         is_tlk_header = 1'b0;
         is_dc_header = 1'b0;

      end
   else
      begin
        
         //////
         /// check the pipeline content in the pervious clock
         
         if( pipeline == 3'b100 && is_tlk_header == 1'b0 )
            is_tlk_header = 1'b1;
            
         if( pipeline == 3'b101 && is_dc_header == 1'b0 )
            is_dc_header = 1'b1;
         
         if( is_masking )
            err = 1'b0;
         else
            err = in_err; 
         
         ///////
         // pipeline movement            

         pipeline = pipeline << 1;
         pipeline[0] = in_err;

         /// tlk
         if( is_tlk_header == 1'b1)
            begin
               if( tlk_cnt < LENGTH_ERR )
                  begin
                     out_tlk_err_bus[tlk_cnt] = err;
                     tlk_cnt = tlk_cnt + 1;
                  end
               else if( tlk_cnt == LENGTH_ERR )
                  got_tlk_err_bus = 1'b1;
            end
      
         
         /// dc
         if( is_dc_header == 1'b1)
            begin
               if( dc_cnt < LENGTH_ERR )
                  begin
                     out_dc_err_bus[dc_cnt] = err;
                     dc_cnt = dc_cnt + 1;
                  end
               else if( dc_cnt == LENGTH_ERR )
                  got_dc_err_bus = 1'b1;
            end
          
      end
end

endmodule