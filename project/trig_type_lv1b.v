/* 
   Top CDT Design
   trig_type_lv1b block
   C. Lin, 2018.03.22, ver 1
   
*/

module trig_type_lv1b
// other variables
(
// input 
  clk               , // system clock
  
  // inputs after alignment and judgement of et
  in_rst            , // reset
  in_ena            , // enable from time control block
  in_lv1a           , // lv1a      input (1  bit )
  in_nclus          , // n cluster input (16 bits)
  
  // trig_type
  user_nclus        ,
  user_prescale_p   ,
  user_prescale_q   ,
  user_ena          ,
  
  // output
  out_lv1b_raw      ,
  out_lv1b_scalar   ,
  raw_cnt           ,
  scalar_cnt      
     
);

input wire         clk;

// inputs
input wire         in_rst;
input wire         in_ena;
input wire         in_lv1a;
input wire [3 :0]  in_nclus;

// register
input wire [15:0]  user_nclus;
input wire [15:0]  user_prescale_p;
input wire [15:0]  user_prescale_q;
input wire         user_ena;

// output
output reg         out_lv1b_raw;
output reg         out_lv1b_scalar;
output reg [23:0]  raw_cnt;
output reg [23:0]  scalar_cnt;

reg        [15:0]  prescale_cnt;

reg is_trig;

always @(posedge clk)
begin
 
// reset counter and pipeline if requested
   if(in_rst==1'b1)
      begin
         raw_cnt = 0;
         scalar_cnt = 0;
         prescale_cnt = 0;
      end

// check whether it satisfies any trigger type
   is_trig = 0;
   if(    in_ena == 1 && user_ena == 1 
       && in_lv1a == 1 && user_nclus[in_nclus] == 1 
     )
      is_trig = 1;

// trigger issuing
   out_lv1b_raw = 1'b0;
   out_lv1b_scalar = 1'b0;
   
   if( is_trig == 1'b1 )
      begin
         out_lv1b_raw = 1'b1;
         if(  prescale_cnt < user_prescale_p )
             begin
                scalar_cnt = scalar_cnt + 1;
                out_lv1b_scalar = 1'b1;
             end
             
         raw_cnt = raw_cnt + 1;
         
         if( prescale_cnt < user_prescale_q - 1 )
            prescale_cnt = prescale_cnt + 1;
         else
            prescale_cnt = 0;
          
      end
      
end

endmodule