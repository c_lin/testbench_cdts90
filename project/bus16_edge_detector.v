/* 
   bus16_edge_detector
*/

module bus16_edge_detector
(
  // input 
  clk               , // system clock

  // inputs
  prebus            ,
  bus               ,
  
  // outputs
  rising_edge       ,
  falling_edge

);


input wire         clk;

// inputs
input wire  [15 :0] prebus;
input wire  [15 :0] bus;

// inputs
output reg          rising_edge;
output reg          falling_edge;

//          

////////////////////////////////////////////
always @(posedge clk) begin

   rising_edge  <= ( prebus==0 && bus>0  ) ? 1'b1 : 1'b0;
   falling_edge <= ( prebus>0  && bus==0 ) ? 1'b1 : 1'b0;
   
end

endmodule