/* 
   Top CDT Design
   C. Lin, 2018.04.06
*/

module spill_cnt
(
// input 
   clk               , // system clock  
   test_mode         , 
   in_live           , // input live
  
// control reg           
   system_rst        , // test mode
   nspill            ,
 
// output
   spill_cnt         ,
   live_disabled          
   
);

input  wire         clk;

input  wire         in_live;
input  wire         system_rst;
input  wire         test_mode;
input  wire [9  :0] nspill;

output reg  [9  :0] spill_cnt;
output reg          live_disabled;

reg                 pre_in_live;

always @(posedge clk)
begin

   live_disabled = 1'b0;

   if( system_rst )
      spill_cnt = 0;

   if( test_mode == 1'b0 )
      begin
         if( pre_in_live == 1'b0 && in_live == 1'b1 )
            spill_cnt = spill_cnt + 1;
   
         if( spill_cnt == nspill && in_live == 1'b0 )
            live_disabled = 1'b1;
   
         pre_in_live = in_live;
      end
end

endmodule