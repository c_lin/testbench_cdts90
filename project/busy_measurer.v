/* 
   busy_measurer
   C. Lin, chiehlin@uchicago.edu
   
   This module is designed to measure the total length for each OFC1 input 
   when DIGOUT is asserted. The counter is reset when the LIVE rising edge 
   is detected.
*/

module busy_measurer
(
// input 
  clk               , // system clock
  
  // inputs
  reset             ,
  busy              ,
  
  // output
  q
);


input wire         clk;

// inputs
input wire         reset;
input wire         busy;

// output
output reg [31 :0] q;

////////////////////////////////////////////
always @(posedge clk) begin

	if( reset == 1'b1 ) begin
	   q <= 0;
	end

   if( busy == 1'b1 ) begin
	   q <= q + 1;
	end
	
end

endmodule
