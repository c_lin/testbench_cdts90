/* 
    reset_FFFFclk
*/

module reset_FFFFclk
(
  //  
  clk               , 
  reset             ,
  
  // outputs
  q

);


input wire        clk;
input wire        reset;

output reg        q;

//
reg        [15:0] cnt = 0;

////////////////////////////////////////////
always @(posedge clk) begin

   if( reset == 1'b1 ) begin
      cnt <= 0;
   end

   if( cnt < 16'hFFFF ) begin
      q <= 1'b1;
      cnt <= cnt + 1;
   end 
   else begin
      q <= 1'b0;
   end

end

endmodule