/* 
   Local CDT Design
  
        
*/

module trig_stat
(
// input 
   clk               , // system clock
   rst               ,
   in_trig           ,
   user_trig_ptn     ,
     
// output
   cnt               ,
             
);

input  wire         clk;
input  wire         rst;
input  wire         in_trig;
input  wire [2  :0] user_trig_ptn;

output reg  [15 :0] cnt;

reg         [3  :0] pipeline;
reg         [1  :0] pulse_cnt;
reg                 got_pulse;

always @(posedge clk)
begin

   if( rst == 1'b1 )
      begin
         cnt = 0;
         pipeline = 0;
         pulse_cnt = 0;
         got_pulse = 1'b0;
      end

   pipeline = pipeline << 1;
   pipeline[0] = in_trig;

   if( got_pulse == 1'b1 )
      begin
         if( pulse_cnt < 3 )
            pulse_cnt = pulse_cnt + 1;
         else
            got_pulse = 1'b0;
      end
      
   if(    got_pulse == 1'b0 
       && pipeline[3]==1'b1 
       && pipeline[2:0] == user_trig_ptn 
     )
      begin
         cnt = cnt + 1;
         got_pulse = 1'b1; 
         pulse_cnt = 0;      
      end
          
end

endmodule