/* 
   Top CDT Design
   lv1b_pipeline block
   C. Lin, 2018.03.28
   
*/

module lv1b_pipeline
// const parameter
#( parameter PIPE = 512 ,
   parameter CONT = 32  
  )
// arguments
(
// input 
  clk               , // system clock
  
  in_rst            , // reset counter
  in_lv1b_type0     , // lv1b input from type block (1  bit )
  in_lv1b_type1     , // lv1b input from type block (1  bit )
  in_lv1b_type2     , // lv1b input from type block (1  bit )
  in_lv1b_type3     , // lv1b input from type block (1  bit )
  in_lv1b_type4     , // lv1b input from type block (1  bit )
  in_lv1b_type5     , // lv1b input from type block (1  bit )
  in_lv1b_type6     , // lv1b input from type block (1  bit )
  in_lv1b_type7     , // lv1b input from type block (1  bit )
   
  user_ps           , // to apply ps or not 
  delay_lv1         , // delay of lv1
  
// output
  out_lv1           , // send to ADC via LVDS
  out_lv1_inhibit   , // send signal to inform lv1a blocking plv1 issuing
  lv1b_cnt          

);

// input
input wire           clk;

input wire           in_rst;
input wire           in_lv1b_type0;
input wire           in_lv1b_type1;
input wire           in_lv1b_type2;
input wire           in_lv1b_type3;
input wire           in_lv1b_type4;
input wire           in_lv1b_type5;
input wire           in_lv1b_type6;
input wire           in_lv1b_type7;

input wire  [7   :0] user_ps;
input wire  [8   :0] delay_lv1;

// output
output reg           out_lv1;
output reg           out_lv1_inhibit;
output reg  [23  :0] lv1b_cnt;

// variables 
reg        [PIPE-1:0] pipeline_lv1b;
reg         [3   :0] do_ps_cnt;
reg         [3   :0] do_not_ps_cnt;

reg         [4   :0] in_lv1b_cnt;
reg         [4   :0] out_lv1b_cnt;
reg        [CONT-1:0] cont_ps; 

////////////////////////////////////////////
always @(posedge clk)
begin

  out_lv1 = 1'b0;
  out_lv1_inhibit = 1'b0;
 
  if( in_rst == 1'b1 )
     begin
        lv1b_cnt      = 24'b0;
        pipeline_lv1b = 512'b0;  
        
        in_lv1b_cnt = 0;
        out_lv1b_cnt = 0;     
     end

// pipeline
   pipeline_lv1b = pipeline_lv1b << 1;
   pipeline_lv1b[0] = 1'b0;
 
   do_ps_cnt = 0;
   do_not_ps_cnt = 0;
   
   if( in_lv1b_type0 == 1'b1 )
      begin
         pipeline_lv1b[0] = 1'b1;
         if( user_ps[0] == 1'b1 )
            do_ps_cnt = do_ps_cnt + 1;
         else
            do_not_ps_cnt = do_not_ps_cnt + 1;
      end
 
   if( in_lv1b_type1 == 1'b1 )
      begin
         pipeline_lv1b[0] = 1'b1;
         if( user_ps[1] == 1'b1 )
            do_ps_cnt = do_ps_cnt + 1;
         else
            do_not_ps_cnt = do_not_ps_cnt + 1;
      end
      
  if( in_lv1b_type2 == 1'b1 )
      begin
         pipeline_lv1b[0] = 1'b1;
         if( user_ps[2] == 1'b1 )
            do_ps_cnt = do_ps_cnt + 1;
         else
            do_not_ps_cnt = do_not_ps_cnt + 1;
      end
      
  if( in_lv1b_type3 == 1'b1 )
      begin
         pipeline_lv1b[0] = 1'b1;
         if( user_ps[3] == 1'b1 )
            do_ps_cnt = do_ps_cnt + 1;
         else
            do_not_ps_cnt = do_not_ps_cnt + 1;
      end
      
  if( in_lv1b_type4 == 1'b1 )
      begin
         pipeline_lv1b[0] = 1'b1;
         if( user_ps[4] == 1'b1 )
            do_ps_cnt = do_ps_cnt + 1;
         else
            do_not_ps_cnt = do_not_ps_cnt + 1;
      end
      
  if( in_lv1b_type5 == 1'b1 )
      begin
         pipeline_lv1b[0] = 1'b1;
         if( user_ps[5] == 1'b1 )
            do_ps_cnt = do_ps_cnt + 1;
         else
            do_not_ps_cnt = do_not_ps_cnt + 1;
      end
      
  if( in_lv1b_type6 == 1'b1 )
      begin
         pipeline_lv1b[0] = 1'b1;
         if( user_ps[6] == 1'b1 )
            do_ps_cnt = do_ps_cnt + 1;
         else
            do_not_ps_cnt = do_not_ps_cnt + 1;
      end
      
  if( in_lv1b_type7 == 1'b1 )
      begin
         pipeline_lv1b[0] = 1'b1;
         if( user_ps[7] == 1'b1 )
            do_ps_cnt = do_ps_cnt + 1;
         else
            do_not_ps_cnt = do_not_ps_cnt + 1;
      end
      
   // do ps condition
   if( pipeline_lv1b[0]==1'b1 )
      begin
         if( do_not_ps_cnt == 0 && do_ps_cnt > 0 ) 
            cont_ps[in_lv1b_cnt] = 1'b1; 
         else
            cont_ps[in_lv1b_cnt] = 1'b0;
            
         in_lv1b_cnt = in_lv1b_cnt + 1;
      end
 
// send inhibit signal to plv1 block (instruction)
// "-4" is the length of any trigger pulse, to avoid overlaying with lv1
   if( pipeline_lv1b[ (delay_lv1-4) +: 8] )
      out_lv1_inhibit = 1'b1;     

   
// send lv1 trigger   
   if( pipeline_lv1b[delay_lv1] == 1'b1 )
      begin
         out_lv1 = 1'b1;
         lv1b_cnt = lv1b_cnt + 1;
      end

// send lv1 w/ ps
   if( pipeline_lv1b[delay_lv1+1] == 1'b1 )
      begin
         if( cont_ps[out_lv1b_cnt] == 1'b1 )
            out_lv1 = 1'b1;
         out_lv1b_cnt = out_lv1b_cnt + 1;
      end
   
      
end

endmodule
