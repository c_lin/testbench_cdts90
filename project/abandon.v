module Abandon(clk,rst,mask,tag0,tag1,tag2,abandon,clear);

input wire clk;
input wire rst;
input wire mask;
input wire tag0;
input wire tag1;
input wire tag2;
input wire [7:0] abandon;

output reg clear;

reg [7:0] cnt_abandon = 8'b11111111;
reg tag_or = 1'b0;
reg tag_and = 1'b0;

always @(posedge clk)
begin

	if(rst)
	begin
		tag_or = 1'b0;
		tag_and = 1'b0;
		clear = 1'b0;
		cnt_abandon = 8'b11111111;
	end

	tag_or = 1'b0;
	tag_or = tag_or | tag0;
	tag_or = tag_or | tag1;
	tag_or = (mask==1'b0)? tag_or | tag2 : tag_or;
	
	tag_and = 1'b1;
	tag_and = tag_and & tag0;
	tag_and = tag_and & tag1;
	tag_and = (mask==1'b0)? tag_and & tag2 : tag_and;
	
	//tag_or = tag0 | tag1 | tag2;
	//tag_and = tag0 & tag1 & tag2;

	if(tag_or==1'b1&&tag_and!=1'b1)
	begin
		cnt_abandon = cnt_abandon + 1'b1;
		if(cnt_abandon==abandon)
		begin
			clear = 1'b1;
		end
	end
	else 
	begin
		cnt_abandon = 8'b11111111;
		clear = 1'b0;
	end	

end

endmodule